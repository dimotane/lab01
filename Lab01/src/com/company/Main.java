package com.company;

import java.sql.SQLOutput;

public class Main {

    public static void main(String[] args) {
	System.out.println("Hello World!");

	System.out.println();

	System.out.println("A \"quoted\" String is");
	System.out.println("'Much' better if you learn");
	System.out.println("The rules of \"Escape Sequences.\"");

	System.out.println();

	System.out.println("Also, \"\" represents an empty String.");
	System.out.println("Don't forget: use \\\" instead of \"!");
	System.out.println("'' Is not the same as \"");

	System.out.println();

	System.out.println("EEEEEEEEEE         A         U          U");
	System.out.println("E                 A A        U          U");
	System.out.println("E                A   A       U          U");
	System.out.println("EEEEEEEEEE      AAAAAAA      U          U");
	System.out.println("E              A       A     U          U");
	System.out.println("E             A         A    U          U");
	System.out.println("EEEEEEEEEE   A            A  UUUUUUUUUUUU");
    }
}
